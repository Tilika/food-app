package com.example.foodorderapp;

import java.util.ArrayList;
import java.util.HashMap;

public class Menu {

    // Singleton
    public static Menu getInstance(){
        if(sharedInstance == null){
            sharedInstance = new Menu();
        }
        return sharedInstance;
    }
    private static Menu sharedInstance = null;
    private Menu(){}

    //Public Method
    public HashMap<String, ArrayList<Dish>> dishesByCuisine(){
        if(menu == null){
            populateMenu();
        }
        return menu;
    }

// Private Property
    private HashMap<String, ArrayList<Dish>> menu = null; // "CuisineName" : [Dish}

    // Private Method
    private void populateMenu(){
        menu = new HashMap<String, ArrayList<Dish>>();

        // Iterate through every possible cuisine type
        for(Cuisine cuisine : Cuisine.values()){
            ArrayList<Dish> dishesList = new ArrayList<Dish>();

            // Insert 6 dishes for each cuisine
            for(int i = 0; i < 6; i++){
                String imageResourceName = cuisine.name().toLowerCase() + "0" + (i+1);
                //System.out.println("DEBUG: this is the file name = " + imageResourceName);
                Dish dish = new Dish(imageResourceName);
                dishesList.add(dish);
            }

            // Disheslist will have 6 dishes added to it
            menu.put(cuisine.name(), dishesList);

        }
        // All populated
        System.out.println("Menu: " + menu);
    }
}
