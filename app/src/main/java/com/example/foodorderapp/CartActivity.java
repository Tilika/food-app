package com.example.foodorderapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import static com.example.foodorderapp.Cart.getInstance;

public class CartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Cart");

        setupTotalTextView();
        setupCheckoutButton();
        setupRecyclerView();
    }

    // Private methods
    @SuppressLint("DefaultLocale")
    private void setupTotalTextView() {
        // XML views
        TextView totalAmountTextView = findViewById(R.id.totalAmountTextView);
        Double total = getInstance().totalPriceInCents() / 100.0;
        totalAmountTextView.setText(String.format("$%.2f", total));
    }

    private void setupCheckoutButton() {
        Button checkoutButton = findViewById(R.id.checkoutButton);
        final Context context = this;
        checkoutButton.setOnClickListener(v -> new AlertDialog.Builder(context)
                .setTitle(R.string.checkout)
                .setMessage(R.string.thank_you_for_your_purchase)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    getInstance().clear();
                    finish();
                })
                .setIcon(R.drawable.ic_baseline_done_24)
                .show());
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        CartAdapter cartAdapter = new CartAdapter(this, getInstance().getCartItems());
        recyclerView.setAdapter(cartAdapter);
    }

    // Adapter for Cart list
    private static class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

        public CartAdapter(@NonNull Context context, @NonNull ArrayList<Cart.CartItem> cartItems) {
            this.context = context;
            this.cartItems = cartItems;
        }

        @NonNull
        @Override
        public CartAdapter.CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_cart, parent, false);
            return new CartViewHolder(itemView);

        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onBindViewHolder(@NonNull CartAdapter.CartViewHolder holder, final int position) {
            Cart.CartItem cartItem = cartItems.get(position);
            holder.quantityTextView.setText(String.format("%sx", cartItem.getQuantity().toString()));
            Double itemPrice = cartItem.itemPriceInCents() / 100.0;
            holder.itemPriceTextView.setText(String.format("$%.2f", itemPrice));
            holder.descriptionTextView.setText(cartItem.dishName());
            Double subtotal = cartItem.subtotalPriceInCents() / 100.0;
            holder.subtotalTextView.setText(String.format("$%.2f", subtotal));
        }

        @Override
        public int getItemCount() {
            return cartItems.size();
        }

        // Private properties
        private final Context context;
        private final ArrayList<Cart.CartItem> cartItems;

        // Cart ViewHolder
        private static class CartViewHolder extends RecyclerView.ViewHolder {

            public View itemView;
            public TextView quantityTextView;
            public TextView itemPriceTextView;
            public TextView descriptionTextView;
            public TextView subtotalTextView;

            public CartViewHolder(@NonNull View itemView) {
                super(itemView);
                this.itemView = itemView;
                quantityTextView = itemView.findViewById(R.id.quantityTextView);
                itemPriceTextView = itemView.findViewById(R.id.itemPriceTextView);
                descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
                subtotalTextView = itemView.findViewById(R.id.subtotalTextView);
            }
        }
    }
}