package com.example.foodorderapp;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RestaurantHeaderFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class RestaurantHeaderFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RestaurantHeaderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RestaurantHeaderFragment newInstance(String param1, String param2) {
        RestaurantHeaderFragment fragment = new RestaurantHeaderFragment();
        return fragment;
    }

    public RestaurantHeaderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_restaurant_header, container, false);
        setupViews();
        return rootView;
    }

    // Private property
    private View rootView;

    // Private Methods
    private void setupViews(){
        Restaurant restaurant = Restaurant.getInstance();

        ImageView imageView = rootView.findViewById(R.id.imageView);
        TextView nameTextView = rootView.findViewById(R.id.name_text_view);
        TextView shortDescriptionTextView = rootView.findViewById(R.id.type_of_food);
        TextView streetAddressTextView = rootView.findViewById(R.id.street_address);
        TextView cityTextView = rootView.findViewById(R.id.city);
        TextView phoneNumberTextView = rootView.findViewById(R.id.phone_num);

        imageView.setImageResource(restaurant.imageResource);
        nameTextView.setText(restaurant.restaurantName);
        shortDescriptionTextView.setText(restaurant.shortDescription);
        streetAddressTextView.setText(restaurant.streetAddress);
        cityTextView.setText(restaurant.city);
        phoneNumberTextView.setText(restaurant.phoneNumber);
    }
}