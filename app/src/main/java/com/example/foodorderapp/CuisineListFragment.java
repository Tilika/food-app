package com.example.foodorderapp;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CuisineListFragment extends Fragment {

    public CuisineListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param cuisine The cuisine for this fragment.
     * @return A new instance of fragment CListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CuisineListFragment newInstance(Cuisine cuisine) {
        CuisineListFragment fragment = new CuisineListFragment();
        Bundle args = new Bundle();
        args.putString(CUISINE_NAME, cuisine.name());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String cuisineName = getArguments().getString(CUISINE_NAME);

            // Get cuisine enum object form cuisine name
            Cuisine cuisine = Cuisine.valueOf(cuisineName);

            // Get all dishes from the menu
            ArrayList<Dish> dishes = Menu.getInstance().dishesByCuisine().get(cuisineName);

            // Save the cuisine
            this.cuisine = cuisine;
            this.dishes = dishes;

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_c_list, container, false);
        setupCuisineName();
        setupRecyclerView();
        return rootView;
    }

    // Private
    private static final String CUISINE_NAME = "cuisine_name";

    private Cuisine cuisine;
    private ArrayList<Dish> dishes;

    private View rootView;
    private RecyclerView recyclerView;

    // Private Methods

    private void setupCuisineName(){
        // Links
        TextView cuisineNameTextView = rootView.findViewById(R.id.cuisine_name_text_view);
        cuisineNameTextView.setText(cuisine.toString());

    }

    private void setupRecyclerView(){
        // Links recycler view
        recyclerView = rootView.findViewById(R.id.recycler_view);

        // Create Linear layout
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

        // set the adapter for the recycler view
        DishAdapter dishAdapter = new DishAdapter(getContext(), dishes);
        recyclerView.setAdapter(dishAdapter);

    }

    private void userTappedOnDishAtPosition(int position){
        // Some debug code
        Dish dish = Menu.getInstance().dishesByCuisine().get(cuisine.name()).get(position);
        System.out.println("User tapped on:" + dish.name);

        // TODO: Navigate dish details screen
    }

    // Adapter for dish objects in recycler
    private class DishAdapter extends RecyclerView.Adapter<DishAdapter.DishViewHolder>{

        public DishAdapter(@NonNull Context context, @NonNull ArrayList<Dish> dishes){
            this.context = context;
            this.dishes = dishes;
        }

        @NonNull
        @Override
        public DishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_dish, parent, false);
            DishViewHolder viewHolder = new DishViewHolder(itemView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull DishViewHolder holder, final int position){
            Dish dish = dishes.get(position);
            holder.nameTextView.setText(dish.name);
            Context context = getContext();
            int id = context.getResources().getIdentifier(dish.imageResourceName, "drawable", context.getPackageName());
            holder.imageView.setImageResource(id);
            holder.itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    // Inform fragment
                    userTappedOnDishAtPosition(position);
                }
            });
        }

        @Override
        public int getItemCount(){return dishes.size();}

        // Private properties
        private final Context context;
        private final ArrayList<Dish> dishes;

        // Dish ViewHolder
        private class DishViewHolder extends RecyclerView.ViewHolder {
            public View itemView;
            public ImageView imageView;
            public TextView nameTextView;

            public DishViewHolder(@NonNull View itemView){
                super(itemView);
                this.itemView = itemView;
                imageView = itemView.findViewById(R.id.imageView);
                nameTextView = itemView.findViewById(R.id.name_text_view);
            }
        }
    }
}